export class Utils {

  public static randomInt(bound: number): number {
    return Math.floor(Math.random() * bound);
  }

  public static toHexString(n: number, length: number = 0): string {
    const str = (n >>> 0).toString(16); // tslint:disable-line:no-bitwise
    const padding = length - str.length;

    if (padding <= 0) {
      return str;
    }

    const parts = Array(padding);

    parts.push(str);

    return parts.join('0');
  }

  // prevent instantiation
  private constructor() {}

}
