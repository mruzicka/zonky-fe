import { Injectable } from '@angular/core';
import { Observable, Unsubscribable } from 'rxjs';
import { HttpClient, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { DataBatch } from './data-batch.model';
import { Utils } from './utils';

export { HttpRequest, HttpHeaders, HttpParams } from '@angular/common/http';
export { DataBatch } from './data-batch.model';


@Injectable({ providedIn: 'root' })
export class ZonkyQueryService {

  constructor(private http: HttpClient) {}

  query(requestBlueprint: HttpRequest<any>): Observable<DataBatch> {
    return new Observable<DataBatch>(
      (observer) => new ZonkyQueryExecutor(requestBlueprint, this.http, observer)
    );
  }

}

class PageRetrievalContext {

  constructor(public page: number, public retrieved: number = 0) {}

}

class ZonkyQueryExecutor implements Unsubscribable {

  static readonly PAGE_SIZE = 1000;
  static readonly FIRST_PAGE_SIZE = 1;

  static readonly MAX_ACTIVE_REQUESTS = 10;

  static readonly HEADER_PAGE_SIZE = 'X-Size';
  static readonly HEADER_PAGE_NUMBER = 'X-Page';
  static readonly HEADER_TOTAL_COUNT = 'X-Total';


  protected done = false;

  protected totalCount = 0;
  protected totalCountUpdateTimestamp = Date.now();

  protected totalRequested = 0;
  protected totalRetrieved = 0;
  protected activeRequests = 0;

  protected requestBlueprint: HttpRequest<any>;
  protected lastPageContext: PageRetrievalContext;


  public static getTotalCount(response: HttpResponse<any>): number {
    const headers = response.headers;

    if (headers) {
      const totalCount = headers.get(ZonkyQueryExecutor.HEADER_TOTAL_COUNT);

      if (totalCount) {
        return Number(totalCount) || 0;
      }
    }

    return 0;
  }


  constructor(requestBlueprint: HttpRequest<any>, protected http: HttpClient, protected observer) {
    this.requestBlueprint = requestBlueprint.clone({
      headers: requestBlueprint.headers.set(ZonkyQueryExecutor.HEADER_PAGE_SIZE, ZonkyQueryExecutor.PAGE_SIZE.toString())
    });

    this.lastPageContext = new PageRetrievalContext(0);

    // send a request with the page size of FIRST_PAGE_SIZE just to get back the total number of entries
    this.submitPageRequest(0, this.lastPageContext, requestBlueprint.clone({
      headers: requestBlueprint.headers
        .set(ZonkyQueryExecutor.HEADER_PAGE_SIZE, Math.min(ZonkyQueryExecutor.FIRST_PAGE_SIZE, ZonkyQueryExecutor.PAGE_SIZE).toString())
    }));
  }

  protected submitPageRequest(
    requested: number,
    context: PageRetrievalContext = this.lastPageContext,
    requestBlueprint: HttpRequest<any> = this.requestBlueprint
  ) {
    // tslint:disable-next-line:no-console
    console.debug(`Submitting request for page: ${context.page}, # of entries requested: ${requested}`);

    const requestTimestamp = Date.now();

    ++this.activeRequests;

    this.http
      .request(requestBlueprint.method, requestBlueprint.url, {
        headers: requestBlueprint.headers
          .set(ZonkyQueryExecutor.HEADER_PAGE_NUMBER, context.page.toString()),
        withCredentials: requestBlueprint.withCredentials,
        params: requestBlueprint.params
          .set('_', Utils.toHexString(Utils.randomInt(2 ** 32), 8)),
        body: requestBlueprint.body,
        observe: 'response'
      })
      .subscribe(
        (response: HttpResponse<any[]>) => {
          --this.activeRequests;
          this.processResponse(requestTimestamp, requested, context, response);
        },
        (errorResponse) => {
          --this.activeRequests;
          this.processErrorResponse(errorResponse);
        }
      );
  }

  protected processResponse(
    requestTimestamp: number,
    requested: number,
    context: PageRetrievalContext,
    response: HttpResponse<any[]>
  ) {
    if (this.done) {
      return;
    }

    let data = response.body;

    if (data.length < requested) {
      this.done = true;
      this.observer.error('Incomplete data received.');
      return;
    }

    const totalCount = ZonkyQueryExecutor.getTotalCount(response);

    if (totalCount > this.totalCount) {
      this.totalCount = totalCount;
      this.totalCountUpdateTimestamp = Date.now();
    } else if (totalCount < this.totalCount && requestTimestamp >= this.totalCountUpdateTimestamp) {
      this.done = true;
      this.observer.error('Total count decreased.');
      return;
    }

    // tslint:disable-next-line:no-console
    console.debug(
      `Retrieved entries from page: ${context.page} ` +
      `offset: ${context.page * ZonkyQueryExecutor.PAGE_SIZE + context.retrieved}, ` +
      `# of retrieved entries: ${data.length - context.retrieved}, ` +
      `total # of retrieved entries: ${this.totalRetrieved + data.length - context.retrieved}`
    );

    if (this.totalCount > this.totalRequested) {
      // total count has risen, we may need to send additional requests

      // the page of the first additional request we may need to send
      let page = Math.floor(this.totalRequested / ZonkyQueryExecutor.PAGE_SIZE);

      if (page === this.lastPageContext.page) {
        // we may need to re-submit the request for the last page requested as it
        // appears the page just got more entries

        // the new number of entries we need to retrieve from the last page
        requested = Math.min(this.totalCount - page * ZonkyQueryExecutor.PAGE_SIZE, ZonkyQueryExecutor.PAGE_SIZE);
        this.totalRequested = page * ZonkyQueryExecutor.PAGE_SIZE + requested;

        if (context.page !== page || data.length < requested) {
          this.submitPageRequest(requested);
        } // else:
          // we got lucky as we are just processing the last requested page response
          // and it turns out that the extra entries we need to retrieve from the last
          // page have already been included in the response

        ++page;
      }

      for (; this.totalRequested < this.totalCount && this.activeRequests < ZonkyQueryExecutor.MAX_ACTIVE_REQUESTS; ++page) {
        requested = Math.min(this.totalCount - this.totalRequested, ZonkyQueryExecutor.PAGE_SIZE);
        this.totalRequested += requested;

        this.lastPageContext = new PageRetrievalContext(page);
        this.submitPageRequest(requested);
      }
    }

    if (context.retrieved > 0) {
      data = data.slice(context.retrieved);
    }

    if (data.length > 0 || this.activeRequests === 0) {
      context.retrieved += data.length;
      this.totalRetrieved += data.length;

      this.observer.next(new DataBatch(data, this.totalCount));

      if (this.totalRetrieved >= this.totalRequested) {
        this.done = true;
        this.observer.complete();
      }
    }
  }

  protected processErrorResponse(errorResponse: any) {
    if (this.done) {
      return;
    }

    let errorMessage;

    if (errorResponse instanceof HttpErrorResponse) {
      const error = errorResponse.error;

      errorMessage = (error instanceof Error)
        ? error.message
        : errorResponse.message;
    } else {
      errorMessage = errorResponse;
    }

    this.done = true;
    this.observer.error(errorMessage);
  }

  public unsubscribe() {
    this.done = true;
  }

}
