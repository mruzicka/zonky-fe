import { Component, Input, Inject, LOCALE_ID } from '@angular/core';
import { formatPercent } from '@angular/common';

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss']
})
export class ProgressBarComponent {

  label: string;
  shift: number;


  constructor(@Inject(LOCALE_ID) protected locale: string) {
    this.progress = 0;
  }

  @Input()
  set progress(progress: number) {
    if (progress < 0) {
      progress = 0;
    } else if (progress > 1) {
      progress = 1;
    }

    this.label = formatPercent(progress, this.locale, '1.2-2');
    this.shift = (1 - progress) * 100;
  }

}
