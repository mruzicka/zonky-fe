import { Component, AfterViewInit, ChangeDetectorRef, ViewChild, ElementRef, TemplateRef, ViewEncapsulation } from '@angular/core';
import { formatDate } from '@angular/common';
import { ZonkyQueryService, HttpRequest, HttpHeaders, HttpParams, DataBatch } from './zonky-query.service';
import { UrlParameterEncodingCodec } from './url-parameter-encoding-codec';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements AfterViewInit {

  static readonly ISO_8601_DATE_FORMAT = 'yyyy-MM-ddTHH:mm:ss.sssZ';

  static readonly URL_PARAMETER_ENCODING_CODEC = new UrlParameterEncodingCodec();


  @ViewChild('ratingInput', {static: false})
  ratingInputRef: ElementRef;

  rating: string;
  averageAmount: number;
  computing = false;
  progress: number;
  errorMessage: string|null = null;


  @ViewChild('errorMessageTemplate', {static: false})
  errorMessageTemplateRef: TemplateRef<any>;
  @ViewChild('progressTemplate', {static: false})
  progressTemplateRef: TemplateRef<any>;
  @ViewChild('resultTemplate', {static: false})
  resultTemplateRef: TemplateRef<any>;

  result: TemplateRef<any>|null = null;


  constructor(private changeDetectorRef: ChangeDetectorRef, private zonkyQueryService: ZonkyQueryService) {}

  compute() {
    this.rating = this.ratingInputRef.nativeElement.value.trim();

    if (this.rating) {
      this.errorMessage = null;

      this.computing = true;
      this.progress = 0;

      this.result = this.progressTemplateRef;

      let totalAmount = 0;
      let totalCount = 0;

      this.zonkyQueryService.query(new HttpRequest('GET', '/zonky/loans/marketplace', {
        params: new HttpParams({
          fromObject: {
            rating__eq: this.rating,
            datePublished__lt: formatDate(Date.now(), AppComponent.ISO_8601_DATE_FORMAT, 'en-US'),
            fields: 'amount'
          },
          encoder: AppComponent.URL_PARAMETER_ENCODING_CODEC
        }),
        headers: new HttpHeaders({
          'X-Order': 'datePublished'
        })
      })).subscribe(
        (dataBatch: DataBatch) => {
          for (const loan of dataBatch.data) {
            totalAmount += Number(loan.amount) || 0;
          }
          totalCount += dataBatch.data.length;
          this.progress = (dataBatch.totalCount === 0)
            ? 1
            : totalCount / dataBatch.totalCount;
        },
        (errorMessage) => {
          this.errorMessage = 'Zonky query failed: ' + errorMessage;
          this.displayResult(this.errorMessageTemplateRef);
        },
        () => {
          if (totalCount > 0) {
            this.averageAmount = totalAmount / totalCount;
            this.displayResult(this.resultTemplateRef, 100);
          } else {
            this.errorMessage = 'No loans with the rating of "' + this.rating + '" found.';
            this.displayResult(this.errorMessageTemplateRef);
          }
        }
      );
    } else {
      this.errorMessage = 'Please enter a rating!';
      this.displayResult(this.errorMessageTemplateRef);
    }
  }

  ngAfterViewInit() {
    this.focusRatingInput();
  }

  private displayResult(template: TemplateRef<any>, delay: number|null = null) {
    if (delay === null) {
      this.computing = false;
      this.result = template;
      this.focusRatingInput();
    } else {
      setTimeout(() => {
        this.displayResult(template);
        this.changeDetectorRef.detectChanges();
      }, delay);
    }
  }

  private focusRatingInput() {
    setTimeout(() => {
      const inputElement = this.ratingInputRef.nativeElement;

      inputElement.setSelectionRange(0, inputElement.value.length);
      inputElement.focus();
    }, 0);
  }

}
