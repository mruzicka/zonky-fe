import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  getHeadlineText() {
    return element(by.css('app-root h2')).getText() as Promise<string>;
  }
}
